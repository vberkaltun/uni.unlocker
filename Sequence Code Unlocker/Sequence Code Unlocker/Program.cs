﻿/* May 22 2016 - PASSWORD UNLOCKER
 * 
 * 
 * This class is used for decoding a sequence code in a list. It is work with 
 * thread library and collection library for to speed up decoding and to avoid 
 * deployment freezing. When an operation is completed it is notify end user.
 * 
 * 
 * This class only works with special library with provided by Pamukkale University. 
 * It does not work with other library models. Other library models will return 
 * incorrect value and return type if used with this class.
 * 
 * 
 * For more information please visit : https://bitbucket.org/vberkaltun/uni.unlocker
 * This class was written by Berk Altun at Pamukkale University, Denizli
 * 2016 - Open Source Software - https://vberkaltun.com
 * 
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using BilgiKontrolLib;

namespace Sequence_Code_Unlocker
{
    class Program
    {
        #region // VARIABLES

        // Type of string for interface operation
        protected const string StringType = "████";

        // Lenght of string for interface operation
        protected const int StringLenght = 4;

        // Default Y cursor of table index
        protected static int CursorY = 0;

        // Default X cursor of table index
        protected static int CursorX = 0;

        // List of unlocked sequence code in order of class - pc - password
        protected static Dictionary<int, string>[] Unlocked = new Dictionary<int, string>[]
        {
            new Dictionary<int, string>(),      // Class 0
            new Dictionary<int, string>(),      // Class 1
            new Dictionary<int, string>(),      // Class 2
        };

        #endregion

        #region // MAIN METHOD

        /// <summary>
        /// Run automatically when code is executed on the process
        /// </summary>
        /// <param name="Args"></param>
        static void Main(string[] Args)
        {
            // Set window size with specified horizontal and vertical variable
            ResizeConsole(80, 30);

            // Add two cell bar to top of console for visual representation
            for (int i = 0; i < 3; i++)
            {
                // Create a new bar on console for visual representation of unlocking
                WriteAt(String.Concat(Enumerable.Repeat(StringType, 20)), 0, i + 0, ConsoleColor.Red, ConsoleColor.DarkRed);

                // Create a new bar on console for visual representation of counting
                WriteAt(String.Concat(Enumerable.Repeat(StringType, 20)), 0, i + 3, ConsoleColor.Gray, ConsoleColor.DarkGray);
            }

            // Hide pointing cursor for a better console view
            Console.CursorVisible = false;

            // Start searching algorithm with new thread for to avoid deployment freezing
            new Thread(new ThreadStart(SearchKey)).Start();
        }

        #endregion

        #region // SEARCHING ALGORITHM

        /// <summary>
        /// Decode a sequence code in an array and then by extension make some specific actions
        /// </summary>
        protected static void SearchKey()
        {
            // Communicate with DLL file and start searching
            BilgiKontrol.baslat("13253004");

            // Start counting 0 to 10.000.000
            for (int Outer = 0; Outer < 10000000; Outer++)
            {
                // Convert keycode to seven bit type and store it
                string Key = Outer.ToString("D7");

                // Start counting 0 to 20
                for (int Inner = 0; Inner < 20; Inner++)
                {
                    // If list of pc laboratory 0 is not contain keycode ...
                    if (!Unlocked[0].ContainsKey(Inner))
                    {
                        // Try keycode for selected pc in laboratory 0
                        if (BilgiKontrol.sifreKontrol(0, Inner, Key) == true)
                        {
                            // Update specified index and horizontal position on interface
                            UpdateInterface(Inner, Key, 0, 02);
                        }
                    }

                    // If list of pc laboratory 1 is not contain keycode ...
                    if (!Unlocked[1].ContainsKey(Inner))
                    {
                        // If condition of pc laboratory 1 is not true ...
                        if (Key.ToCharArray().Distinct().ToArray().Count() == Key.ToCharArray().Count())
                        {
                            // Try keycode for selected pc in laboratory 1
                            if (BilgiKontrol.sifreKontrol(1, Inner, Key) == true)
                            {
                                // Update specified index and horizontal position on interface
                                UpdateInterface(Inner, Key, 1, 28);
                            }
                        }
                    }

                    // If list of pc laboratory 2 is not contain keycode ...
                    if (!Unlocked[2].ContainsKey(Inner))
                    {
                        // Create a flag for keycode controlling
                        bool Flag = true;

                        // Start counting 0 to lenght - 1
                        for (int i = 0; i < Key.Length - 1; i++)
                        {
                            // If desired conditions of pc laboratory 2 is false ...
                            if (Key[i] == Key[i + 1] || Key[i] + 1 == Key[i + 1] || Key[i] == Key[i + 1] + 1)
                            {
                                // Change flag register value as false 
                                Flag = false;

                                // Break code block for other comparison tests
                                break;
                            }
                        }

                        // If desired conditions of pc laboratory 2 is true ...
                        if (Flag == true)
                        {
                            // Try keycode for selected pc in laboratory 2
                            if (BilgiKontrol.sifreKontrol(2, Inner, Key) == true)
                            {
                                // Update specified index and horizontal position on interface
                                UpdateInterface(Inner, Key, 2, 54);
                            }
                        }
                    }

                    // If all passwords of all pc laboratory are broken ...
                    if (Unlocked[0].Count == 20 && Unlocked[1].Count == 20 && Unlocked[2].Count == 20)
                    {
                        // Clear console window
                        Console.Clear();

                        // Set cursor 0 to 0
                        WriteAt("", 0, 0);

                        // Communicate with DLL file and stop searching
                        BilgiKontrol.bitir();

                        // Show pointing cursor for user input
                        Console.CursorVisible = true;

                        // Get a key from user and finish 
                        Console.ReadKey();
                    }
                }
            }
        }

        /// <summary>
        /// Update a variable on interface with specified index and horizontal position
        /// </summary>
        /// <param name="Inner"></param>
        /// <param name="Key"></param>
        /// <param name="Index"></param>
        /// <param name="Horizontal"></param>
        /// <returns></returns>
        protected static bool UpdateInterface(int Inner, string Key, int Index, int Horizontal)
        {
            try
            {
                // Add unlocked key to dictionary list array
                Unlocked[Index].Add(Inner, Key);

                // Fill in pointed cell on user interface with green color
                WriteAt(StringType, Inner * StringLenght, Index, ConsoleColor.Green, ConsoleColor.DarkGreen);

                // Update total count of remaining locked key on user interface 
                WriteAt("LAB " + Index + " Kalan PC :      " + BilgiKontrol.kalanSifreSayisi(Index).ToString("D2"), Horizontal, 4, ConsoleColor.DarkGray, ConsoleColor.Gray);

                // Add green flag to user interface for unlocked key
                WriteAt("PCLAB" + Index + " : " + Inner.ToString("D2") + "      " + Key, Horizontal, 8 + Inner);

                // Return true to sender method when successful execution
                return true;
            }
            catch (Exception)
            {
                // Notify end user when error is triggered
                Console.WriteLine("Error: UpdateInterface");

                // Return false to sender method when unsuccessful execution
                return false;
            }
        }

        #endregion

        #region // CONSOLE METHOD

        /// <summary>
        /// Set window size to desired best fit size for operation 
        /// </summary>
        protected static bool ResizeConsole(int Horizontal, int Vertical)
        {
            try
            {
                // Sets window position to upper left
                System.Console.SetWindowPosition(0, 0);

                // Make sure buffer is bigger than window size
                System.Console.SetBufferSize(Horizontal, Vertical);

                // Set window size to best fit size
                System.Console.SetWindowSize(Horizontal, Vertical);

                // Return true to sender method when successful execution
                return true;
            }
            catch (ArgumentOutOfRangeException)
            {
                // Notify end user when error is triggered
                Console.WriteLine("Error: ConsoleRebuild");

                // Return false to sender method when unsuccessful execution
                return false;
            }
        }

        /// <summary>
        /// Write incoming variable to desired cursor with console color
        /// </summary>
        /// <param name="Variable"></param>
        /// <param name="Horizontal"></param>
        /// <param name="Vertical"></param>
        /// <param name="ForegroundColor"></param>
        /// <param name="BackgroundColor"></param>
        /// <returns></returns>
        protected static bool WriteAt(string Variable, int Horizontal, int Vertical, ConsoleColor ForegroundColor, ConsoleColor BackgroundColor)
        {
            try
            {
                // Change console foreground color
                Console.ForegroundColor = ForegroundColor;

                // Change console background color
                Console.BackgroundColor = BackgroundColor;

                // Write incoming variable to desired cursor
                WriteAt(Variable, Horizontal, Vertical);

                // Return true to sender method when successful execution
                return true;
            }
            catch (ArgumentOutOfRangeException)
            {
                // Notify end user when error is triggered
                Console.WriteLine("Error: WriteAt");

                // Return false to sender method when unsuccessful execution
                return false;
            }
        }

        /// <summary>
        /// Write incoming variable to desired cursor without console color
        /// </summary>
        /// <param name="Variable"></param>
        /// <param name="Horizontal"></param>
        /// <param name="Vertical"></param>
        protected static bool WriteAt(string Variable, int Horizontal, int Vertical)
        {
            try
            {
                // Set cursor to horizontal and vertical position
                Console.SetCursorPosition(CursorX + Horizontal, CursorY + Vertical);

                // Append or write new variable
                Console.Write(Variable);

                // Reset color after interface operation
                Console.ResetColor();

                // Return true to sender method when successful execution
                return true;
            }
            catch (ArgumentOutOfRangeException)
            {
                // Notify end user when error is triggered
                Console.WriteLine("Error: WriteAt");

                // Return false to sender method when unsuccessful execution
                return false;
            }
        }

        #endregion
    }
}
